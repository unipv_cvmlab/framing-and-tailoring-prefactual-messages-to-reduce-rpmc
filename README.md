# Framing and Tailoring Prefactual Messages to Reduce Red Meat Consumption: Predicting Effects through a Psychology-Based Graphical Causal Model

### Patrizia Catellani, Valentina Carfora, Marco Piastra

## Supplementary Material

This repository contains supplementary material for the article above.

1. File `FRAMEAT_EXT3_frontiers.csv`
   Comma-separated text file including raw experimental data.
   
2. File `FRAMEAT_EXT3_AUTO_n6c0_n5c0_frontiers.xdsl`  
   XML file containing the complete definition of the Dynamic Bayesian Network (DBN) automatically elicited and trained from the raw data above  
   (see the article for further explanation).
   
> NOTE: the `.xsdl` file is in the format used in Genie Modeler by BayesFusion, LLC  
> (see https://www.bayesfusion.com/genie/)

